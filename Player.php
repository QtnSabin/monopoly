<?php 

Class Player 
{
    public $id;
    public $name;
    public $money = 2000;
    public $position = 0;

    public function __construct($id, $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    /**
     * Met à jour la position du joueur sur le plateau.
     */
    public function updatePosition($increment)
    {
        if ($this->position + $increment < 40) {
            $this->position += $increment;
        } elseif ($this->position + $increment = 40) {
            $this->position = 0;
            $this->money += 200;
            echo 'Vous avez gagné 200M en atterissant sur la case départ, il se solde maintenant à ' . $this->money;
            echo("\r\n");
        } else {
            $total = $this->position + $increment;
            $this->position = $total - $increment - 1;
            $this->money += 100;
            echo 'Vous avez gagné 100M en passant par la case départ, il se solde maintenant à ' . $this->money;
            echo("\r\n");            
        }
    }
}