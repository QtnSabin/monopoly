<?php

include_once('./Box.php');
include_once('./BoxType.php');
include_once('./Card.php');
include_once('./CardType.php');

Class Board
{
    public $boxes = [];
    public $communityCards = [];
    public $chanceCards = [];

    public function __construct()
    {
        $this->setBoxes();
        $this->setCards();
    }
    /**
     * Function d'interaction avec les cases
     */
    public function boxInteraction($player, $box) 
    {
        if ($box->boxType->name == "property") {
            if ($box->owner == null && $box->price != null) {
                echo 'Cette propriétée est disponible à la vente pour : ' . $box->price;
                echo("\r\n");
                $buyResponse = readline('Voulez vous acheter la propriété : ' . $box->name . '(y/n) ?');
                echo("\r\n");
                if ($buyResponse === 'y') {
                    if ($player->money >= $box->price) {
                        $player->money -= $box->price;
                        echo 'Vous avez été débité de ' . $box->price . ', votre solde est maintenant de ' . $player->money;
                        echo("\r\n");
                        $box->owner = $player;
                    } else {
                        echo 'Fond insuffisant pour acheter ce bien';
                        echo("\r\n");
                    }
                } else {
                    echo 'D\'accord votre tour est maintenant terminé';
                    echo("\r\n");
                }
            } else {
                $this->rentPayement($player, $box);
            }
        } elseif ($box->boxType->name == "special") {
            switch ($box->name) {
                case 'Départ':
                break;
                case 'Prison':
                    echo 'Visite simple de la prison, Wouah quelle belle vue';
                    echo("\r\n");
            
                break;
                case 'Parking':
                    echo 'Vous êtes au parking gratuit reposez vous bien';
                    echo("\r\n");
            
                break;
                case 'Allez en Prison':
                    echo 'Un passant vous à vue. Vraiment, exhiber votre piou piou devant une école maternelle ? Vous poussez le cosplay de M-Jackson un peu loin, allez en prison !';
                    echo("\r\n");
                    $player->position = 10;
                break;
            }
        } elseif ($box->boxType->name == "card") {
            switch ($box->name) {
                case 'Chance':
                    $pickedChanceCard = $this->pickChanceCard($player);
                    echo 'Vous avez pioché une carte chance : ' . $pickedChanceCard->cardText;
                    echo("\r\n");
                break;
                case 'Caisse de communauté':
                    $pickedCommunityCard = $this->pickCommunityCard($player);
                    echo 'Vous avez pioché une carte communauté : ' . $pickedCommunityCard->cardText;
                    echo("\r\n");
                break;
                case 'Impôts sur le revenu':
                    $player->money -= $box->price;
                    echo('EHEH merci le contribuable payez 200M, les voyages me coûtent chère. Bisous Manu  ');
            }
        }
    }

    /**
     * Création des cases.
     */
    public function setBoxes()
    {
        $boxTypeNames = [
            'property',
            'card',
            'special'
        ];

        foreach ($boxTypeNames as $value) {
            $boxTypes[$value] = new BoxType($value);
        }
        
        include('./data.php');
        $boxes = $boxesList;

        foreach ($boxes as $box) {
            array_push($this->boxes, new Box($box['name'], $box['boxType'], $box['price'], $box['color'], $box['owner'], $box['salary'], $box['house'], $box['housePrice'], $box['mortageValue']));
        }
    }
    
    /**
     * Récupère la case à la position du joueur
     */
    public function getBox($position)
    {
        return $this->boxes[$position];
    }
    
    /**
     * Récupère toutes les cases appartenant au joueur.
     */
    public function getPlayerBoxes($player) 
    {
        $playerBoxes = [];
        
        foreach ($this->boxes as $box) {
            if($box->owner == $player) {
                array_push($playerBoxes, $box);
            }
        }

        return $playerBoxes;
    }

    /**
     * Création des cartes communautés et chances.
     */
    public function setCards()
    {
        $cardTypeNames = [
            'chance',
            'community'
        ];

        foreach ($cardTypeNames as $value) {
            $cardTypes[$value] = new CardType($value);
        }

        include('./data.php');

        $chanceCards = $chanceCardList;

        $communityCards = $communityCardList;

        foreach ($chanceCards as $card) {
            array_push($this->chanceCards, new Card($card['cardType'], $card['cardEffect'], $card['cardText']));
        }

        foreach ($communityCards as $card) {
            array_push($this->communityCards, new Card($card['cardType'], $card['cardEffect'], $card['cardText']));
        }
    }

    /**
     * Paiement du loyer.
     */
    public function rentPayement($player, $box) 
    {
        $boxOwner = $box->owner;
        $boxRent = $box->salary[$box->house];

        if($player->money >= $boxRent && $boxOwner != $player) {
            echo ($player->name . ' a donné ' . $boxRent . 'M à ' . $boxOwner->name . '.');
            echo("\r\n");
            $player->money -= $boxRent;
            echo ($player->name . ' a désormais ' . $player->money . 'M.');
            echo("\r\n");
            $boxOwner->money += $boxRent;
            echo ($boxOwner->name . ' a désormais ' . $boxOwner->money . 'M.');
            echo("\r\n");
        }
        else {
            $this->mortage($player, $boxRent, $boxOwner);
        }
    }

    /**
     * Incapacité de paiement.
     */
    public function mortage($player, $price, $boxOwner) {
        echo ($player->name . ' n\' a pas assez d\'argent pour payer (' . $price . 'M).');
        echo("\r\n");
        
        $playerBoxes = $this->getPlayerBoxes($player);

        if (count($playerBoxes) > 0) {
            while ($player->money < $price && count($playerBoxes) > 0) {
                echo $player->name . ' a ' . $player->money . 'M.';
                echo("\r\n");

                foreach ($playerBoxes as $key => $box) {
                    echo ('Propriété (' . $key . ') ' . $box->name . ', prix de l\'hypothèque : ' . $box->mortageValue);
                    echo("\r\n");
                }

                $mortageResponse = readline('Entrez le numéro de la propriété à vendre : ');
                if ($playerBoxes[$mortageResponse]->owner == $player) {
                    $mortageBox = $this->boxes[array_search($playerBoxes[$mortageResponse], $this->boxes)];
                    $mortageBox->owner = null;

                    array_splice($playerBoxes, $mortageResponse, 1);
                    $player->money += $mortageBox->mortageValue;
                    
                    echo 'Propriété vendu.';
                    echo("\r\n");
                } else {
                    echo 'Cette propriété n\'appartient pas au joueur.';
                    echo("\r\n");
                }
            }

            if ($player->money >= $price) {
                $boxOwner->money += $price;
                $player->money -= $price;

                echo ($player->name . ' a donné ' . $price . 'M à ' . $boxOwner->name . '.');
                echo("\r\n");
            } else {
                $player->money = -999999;
            }
        } else {
            $player->money = -999999;
        }
    }

    /**
     * Pioche une carte chance.
     */
    public function pickChanceCard($player) 
    {
        $cardPicked = array_shift($this->chanceCards);

        return $cardPicked;

        array_push($this->chanceCards, $cardPicked);
    }

    /**
     * Pioche une carte communauté.
     */
    public function pickCommunityCard($player) 
    {
        $cardPicked = array_shift($this->communityCards);
        
        echo($cardPicked->text);
        $this->applyPickedCard($cardPicked, $player);
        return $cardPicked;

        array_push($this->communityCards, $cardPicked);
    }

    /**
     * Applique les effets de la carte sur le joueur.
     */
    public function applyPickedCard($card, $player) 
    {
        $cardEffect = $card->cardEffect;
        switch ($cardEffect) {
            case ($cardEffect == -3 || $cardEffect == 39 || $cardEffect == 10):
                $player->position = $card->cardEffect;
            break;
            case ($cardEffect > 39 || $cardEffect < -10):
                $player->money += $card->cardEffect;
            break;
        }
    }
}

