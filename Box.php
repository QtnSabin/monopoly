<?php

Class Box 
{
    public $name;
    public $boxType;
    public $price;
    public $color;
    public $owner;
    public $salary;
    public $house;
    public $housePrice;
    public $mortageValue;

    public function __construct($name, $boxType, $price, $color, $owner, $salary, $house, $housePrice, $mortageValue)
    {
        $this->name = $name;
        $this->boxType = $boxType;
        $this->price = $price;
        $this->color = $color;
        $this->owner = $owner;
        $this->salary = $salary;
        $this->house = $house;
        $this->housePrice = $housePrice;
        $this->mortageValue = $mortageValue;
    }
}