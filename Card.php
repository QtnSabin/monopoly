<?php 

Class Card 
{
    public $cardType;
    public $cardEffect;
    public $cardText;

    public function __construct($cardType, $cardEffect, $cardText)
    {
        $this->cardType = $cardType;
        $this->cardEffect = $cardEffect;
        $this->cardText = $cardText;
    }
}

