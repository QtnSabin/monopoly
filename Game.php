<?php

include_once('./Board.php');
include_once('./Player.php');

Class Game {
    public $players = [];
    public $classement = [];
    public $board;
    
    public function __construct()
    {
        $this->playGame();
    }
 
    /**
     * Défini les étapes du jeu. 
     */
    public function playGame() 
    {
        $this->setPlayers();
        $this->generateBoard();
        $this->randomStart();
        
        while (count($this->players) > 1) {
            foreach ($this->players as $player) {
                $this->play($player);
                echo("\r\n");            
                readline('Fin du tour');
                echo("\r\n");            
            }
        }

        echo ($this->players[0]->name . ' a gagné la partie avec ' . $this->players[0]->money . 'M.');
    }

    /**
     * Création des joueurs.
     */
    public function setPlayers()
    {
        $playerNumber = readline('Nombre de joueurs : ');

        while ($playerNumber < 2) {
            echo 'Nombre de joueurs insuffisant.';
            echo("\r\n");
            $playerNumber = readline('Nombre de joueurs : ');
            echo("\r\n");
        }

        for ($i=1; $i <= $playerNumber; $i++) {
            $playerName = readline('Nom du joueur ' . $i . ': ');
            echo("\r\n");
            array_push($this->players, new Player($i, $playerName));
        }
    }

    public function deletePlayer($player)
    {
        array_splice($this->players, array_search($player, $this->players), 1);
    }

    /**
     * Génération du plateau de jeu.
     */
    public function generateBoard()
    {
        $this->board = new Board();
    }   

    /**
     * Détermine le joueur qui jouera en premier.
     */
    public function randomStart()
    {
        $winners = $this->players;

        //Permet d'obtenir le joueur ayant obtenue le plus grand nombre
        while (count($winners) != 1) {
            $score = 0;
            foreach ($winners as $index => $winner) {
                $newScore = $this->dicesRoll();
                if ($newScore > $score) {
                    $score = $newScore;
                    array_splice($winners, 0, (int) $index);
                } elseif ($newScore < $score) {
                    unset($winners[$index]);
                }
            }
        }

        echo($winners[0]->name . ' a gagné le tirage au sort pour débuter la partie.');
        echo("\r\n");

        // Réordonne l'ordre des joueurs en fonction du résultat des lancés de dés.
        foreach (array_splice($this->players, 0, array_search($winners[0], $this->players)) as $player) {
            array_push($this->players, $player);
        }

        return $winners[0];
    }

    /**
     * Renvoie un nombre aléatoire entre 1 et 12.
     */ 
    public function dicesRoll()
    {
        return random_int(2, 12);
    }

    /**
     * Actions du joueur.
     */
    public function play($player)
    {
        echo($player->name . ' lance les dés.');
        echo("\r\n");

        $score = 2;
        echo($player->name . ' avance de ' . $score . ' cases.');
        echo("\r\n");

        $player->updatePosition($score);
        $box = $this->board->getBox($player->position);
        
        echo($player->name . ' est tombé sur la case : ' . $box->name);
        echo("\r\n");

        $this->board->boxInteraction($player, $box);

        if ($player->money < 0) {
            $this->deletePlayer($player);
        }
    }
}
